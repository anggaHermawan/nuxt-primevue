import carsLarge from "@/static/data/cars-large.json";
import carsMedium from "@/static/data/cars-medium.json";
import carsSmall from "@/static/data/cars-small.json";

export default class CarService {
  getCarsSmall() {
    return carsSmall.data;
  }

  getCarsMedium() {
    return carsMedium.data;
  }

  getCarsLarge() {
    return carsLarge.data;
  }
}
