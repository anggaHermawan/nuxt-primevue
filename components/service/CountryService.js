import data from "@/static/data/countries.json";
export default class CountryService {
  getCountries() {
    return data.data;
  }
}
