import data from "@/static/data/events.json";

export default class EventService {
  getEvents() {
    return data.data;
  }
}
