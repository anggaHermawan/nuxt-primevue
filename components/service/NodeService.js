import treetablenodes from "@/static/data/treetablenodes.json";
import filesystem from "@/static/data/filesystem.json";
import treenodes from "@/static/data/treenodes.json";

export default class NodeService {
  getFilesystem() {
    return filesystem.data;
  }

  getTreeNodes() {
    return treenodes.data;
  }

  getTreeTableNodes() {
    return treetablenodes.data;
  }
}
